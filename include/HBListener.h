/** Gravity Heartbeat CallBack Header.
 \file HBListener.h
 \author Fred Livingston
 \date 28 Feb 2019
 \version 0.1
 \brief Sample Code of using Garvity Publisher Service
 \copyright Copyright(C) 2019 MechaSpin. All Rights Reserved.                                                            **
 */

#ifndef HBLISTENER_H
#define HBLISTENER_H

#include <GravityNode.h>
#include <GravityLogger.h>

using namespace gravity;
using namespace std;

class HBListener : public GravityHeartbeatListener
{
public:
	virtual void MissedHeartbeat(std::string dataProductID, int64_t microsecond_to_last_heartbeat, int64_t& interval_in_microseconds);
	virtual void ReceivedHeartbeat(std::string dataProductID, int64_t& interval_in_microseconds);
};


#endif
