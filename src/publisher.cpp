/** Gravity Publisher Main Function.
 \file publisher.cpp
 \author Fred Livingston
 \date 28 Feb 2019
 \version 0.1
 \brief Sample Code of using Garvity Publisher Service
 \copyright Copyright(C) 2019 MechaSpin. All Rights Reserved.                                                            **
 */
 
#include <iostream>
#include <GravityNode.h>
#include <GravityLogger.h>
#include <Utility.h>
#include <MooringPoseDataProduct.pb.h>
#include "HBListener.h"

#define HEART_BEAT_INTERVAL 500000

using namespace gravity;
using namespace std;


int main()
{
	GravityNode gn;
    HBListener hbl;
	
	//Initialize gravity, giving this node a componentID.
	gn.init("AMS_Gravity_Node");

	gn.registerDataProduct("MooringPoseDataProduct", GravityTransportTypes::TCP);
	gn.registerHeartbeatListener("CLAM_Gravity_Node", HEART_BEAT_INTERVAL, hbl);


	bool quit = false; //TODO: set this when you want the program to quit if you need to clean up before exiting.
	int count = 1;
	while(!quit)
	{
		//Create a data product to send across the network of type "MooringPoseDataProduct".
		GravityDataProduct mooringPoseDataProduct("MooringPoseDataProduct"); 
	
		//Initialize our message
		MooringPoseDataProductPB mooringPoseDataPB;
     
		
        //TODO: Query actual Data, Note: Dummy Data for Now            
        mooringPoseDataPB.mutable_header()->set_station_id(HeaderPB::FWD);   // Forward Morring Station	
        mooringPoseDataPB.mutable_header()->set_module_id(HeaderPB::MM_1);   // Morring MOdule 1	
        mooringPoseDataPB.mutable_header()->set_location_id(HeaderPB::PAD);  // PAD	
		mooringPoseDataPB.set_x_mm(count);
        mooringPoseDataPB.set_y_mm(count);
        mooringPoseDataPB.set_z_mm(count);
               

		//Put message into data product
		mooringPoseDataProduct.setData(mooringPoseDataPB);

		//Publish the data product.
		gn.publish(mooringPoseDataProduct);

		//Increment count
        count = (count >= 50) ? 1 : count + 1;
	
		//Sleep for 1 second.
		gravity::sleep(1000);
	}
}
