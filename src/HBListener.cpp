/** Gravity Heartbeat CallBack Function.
 \file HBListener.cpp
 \author Fred Livingston
 \date 28 Feb 2019
 \version 0.1
 \brief Sample Code of using Garvity Publisher Service
 \copyright Copyright(C) 2019 MechaSpin. All Rights Reserved.                                                            **
 */

#include "HBListener.h"

void HBListener::MissedHeartbeat(std::string dataProductID, int64_t microsecond_to_last_heartbeat, int64_t& interval_in_microseconds)
{
	Log::warning("Missed Heartbeat.  Last heartbeat %d microseconds ago.  ", microsecond_to_last_heartbeat);
}

void HBListener::ReceivedHeartbeat(std::string dataProductID, int64_t& interval_in_microseconds)
{
	Log::warning("Received Heartbeat.");
}
